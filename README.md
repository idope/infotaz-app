InfoTaz
===================

InfoTaz est une application hybride qui rassemble différentes bases de données de drug testing (dans les pays où ils sont autorisés).


> **Notes:**

> - Le projet entre dans le cadre de la politique de Santé Publique dite de **"Réduction des risques liés à l'usage de drogues"** *(Loi du 9 août 2004)*.
> - Vous pouvez apporter votre pierre à l'édifice et participer au projet, il y a toujours à faire !


Sommaire
-------------

[TOC]

-------

Informations et principes
-------------

Le but de l'application InfoTaz est de fournir un moyen rapide, fiable et pratique pour diffuser des alertes à l'attention des potentiels consommateurs de stupéfiants de type Ecstasy.

L'application se limitera uniquement aux stupéfiants ou dérivés qui sont connu pour être présenté sous forme de cachets. 

En effet bien que le principal composant connu des cachets soit la MDMA, ceux-ci sont souvent mélangé avec d'autres principes qui peuvent s'avérer dangereux.

Nous rassemblons dans notre base commune donnée APiDope(https://gitlab.com/idope/apidope) les informations de plusieurs sites sérieux.

Fonctionnalitées
-------------

* azettete
* tetaze

A venir :

* azettete
* tetaze

Sources des informations
-------------
Voici les différentes sources d'informations qui sont actuellement intégrées, en cours d'intégration, ou en attente.

| Site           | Site                              | Lanque| API
| :-------       | ----:                             | :---: |:---:
| NuitBlanche    | http://                           |  BE  | KO
| Pill Report    | http://www.pillreports.net        |  EN  | En cours
| Danno.CH       | http://www.danno.ch/drugchecking  |  CH  | KO
| Erowid         | https://www.erowid.org            |  EN  | KO
| Modus Vivendi  | http://www.modusvivendi-be.org    |  BE  | KO
| Psychonaut     | http://www.psychonaut.com         |  FR  | KO
| Techno +       | http://www.technoplus.org         |  FR  | KO

Et plus encore ! N'hésitez pas à rajouter un site afin que nous étudions la meilleure façon de l'intégrer à l'application.


Comment contribuer
-------------

[Plus d'informations](CONTRIBUTING)


### Données
### Développements



Technologies
-------------

### Application
Developed with Ionic Framework (AngularJS, NodeJS, Cordova)

http://ionicframework.com


### API

[Voir iDope-API](https://gitlab.com/idope/idope-api)


### Sites

Méthodes de développements
-------------

### Normes et bonnes pratiques
### Merge Request 


Licence
--------------
[Plus d'informations](LICENSE)
