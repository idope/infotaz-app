
// angular.module is a global place for creating, registering and retrieving Angular modules
angular.module('ionicApp', ['ionic', 'starter.controllers', 'starter.services', 'starter.directives', 'pascalprecht.translate', 'ngCordova', 'jett.ionic.filter.bar', 'slugifier'])

    .run(function ($ionicPlatform, $rootScope, $ionicScrollDelegate, $state, $cordovaNetwork, $ionicPopup) {

        $ionicPlatform.ready(function () {

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                $rootScope.isOnline = false;

                try {
                    $rootScope.isOnline = $cordovaNetwork.isOnline();
                }
                catch (e) { // For debugging in the browser
                    $rootScope.isOnline = true;
                }
            }

            /************************************/
            /* VARIABLES                        */
            /************************************/

            $rootScope.settings = {
                'languages' : [{
                    'prefix':'en',
                    'name':'English'
                },{
                    'prefix':'fr',
                    'name':'Francais'
                }]
            };

        });
    })

    .config(function ($ionicConfigProvider, $stateProvider, $urlRouterProvider, $translateProvider, $ionicFilterBarConfigProvider) {

        $ionicConfigProvider.views.maxCache(0);
        $ionicConfigProvider.tabs.position('bottom');
        $ionicConfigProvider.tabs.style('striped');
        $ionicConfigProvider.navBar.alignTitle('center');
        $ionicConfigProvider.backButton.text('Retour').icon('ion-chevron-left');

        $ionicFilterBarConfigProvider.placeholder('Rechercher par motif');
        /************************************/
        /* ROUTER                        */
        /************************************/
        $stateProvider

            .state('introduction', {
                url: '/introduction',
                templateUrl: 'templates/introduction.html',
                controller: 'IntroductionCtrl'
            })
            .state('logout', {
                url: '/logout',
                controller: 'LogoutCtrl'
            })
            /* ONCE ENTER IN */
            .state('tabs', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs/tabs.html'
            })
            .state('tabs.discover', {
                url: '/discover',
                views: {
                    'tab-discover': {
                        templateUrl: 'templates/tabs/discover.html',
                        controller: 'DiscoverCtrl'
                    }
                }
            })
            .state('tabs.detail', {
                url: "/detail/:itemId",
                views: {
                    'tab-detail': {
                        controller: 'DetailCtrl',
                        templateUrl: "templates/tabs/detail.html"
                    }
                }
            })
            .state('tabs.about', {
                url: '/about',
                views: {
                    'tab-about': {
                        templateUrl: 'templates/tabs/about.html',
                        controller: 'AboutCtrl'
                    }
                }
            })
            .state('tabs.help', {
                url: '/help',
                views: {
                    'tab-help': {
                        templateUrl: 'templates/tabs/help.html',
                        controller: 'HelpCtrl'
                    }
                }
            })


        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/introduction');

    })
