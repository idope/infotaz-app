angular.module('starter.controllers', [])

    .controller('IntroductionCtrl', function ($scope, $state, $ionicHistory) {
        $ionicHistory.clearHistory();
        $scope.hideBackButton = true;


    })

    .controller('LogoutCtrl', function ($scope, $state, $ionicHistory, $rootScope) {
        $ionicHistory.clearHistory();
        $scope.hideBackButton = true;

        $rootScope.show('Fermeture..');
        $ionicHistory.clearCache();

        ionic.Platform.exitApp()


    })

    .controller('AboutCtrl', function ($scope, $rootScope, $state, $translate, $ionicHistory, $ionicActionSheet) {
        console.log('tetestabout');
    })

    .controller('DetailCtrl', function ($scope, $rootScope, $state, $translate, $ionicHistory, $ionicActionSheet, Items, $stateParams) {
        console.log('detailsctrl');
        $scope.allItems = Items.all();
console.log($scope.allItems[2]);
        // $scope.item  = Items.get($stateParams.itemId);
        $scope.item  = $scope.allItems[2]

        console.log($scope.item);

    })

    .controller('HelpCtrl', function ($scope, $rootScope, $state, $translate, $ionicHistory, $ionicActionSheet) {
        console.log('help');
    })

    .controller('DiscoverCtrl', function ($scope, $rootScope, $state, $translate, $ionicHistory, $http, $ionicPlatform, Items, $ionicFilterBar, $ionicPopup) {

        var filterBarInstance;

        $scope.showFilterBar = function () {
            filterBarInstance = $ionicFilterBar.show({
                items: $scope.allItems,
                update: function (filteredItems, filterText) {
                    $scope.items = filteredItems;
                    if (filterText) {
                        console.log(filterText);
                    }
                },
                filterProperties: ['name'],
                cancelText:"Fermer"
            });
        };

        // get from service (localstorage)
        $scope.allItems = Items.all();

        $scope.items = $scope.allItems.slice(0,10);

        // si vide
        if(!Object.keys($scope.items).length) {

            console.log('récupération des objets');

            $http.get('clean.saferparty.json')
                .success(function (data) {
                    // The json data will now be in scope.
                    $scope.items = data;

                    Items.save(data);

                    $scope.showToast = function() {
                        $cordovaToast
                            .show("Données mises à jours", 'short', 'center')
                            .then(function(success) {
                                console.log('Success');
                            }, function (error) {
                                console.log('Error');
                            });
                    };


                })
                .error(function (data) {
                    $scope.doRefresh();
                })
            ;
        }

        $scope.doRefresh = function() {

            if (!$rootScope.isOnline) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Pas de connexion',
                    template: 'Vous devez activer votre connexion Internet'
                });
                alertPopup.then(function() {

                });

            }

            $http.get('http://localhost/clean.saferparty.json')
                .success(function(newItems) {
                    $scope.items = newItems;

                    Items.save(newItems);
                })
                .finally(function() {
                    // Stop the ion-refresher from spinning
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };

        var offsetX = 0;
        var offsetY = 10;

        $scope.loadMore = function() {

            offsetX = offsetX + 10;
            offsetY = offsetY + 10;

            var newItems = $scope.allItems.slice(offsetX,offsetY);

            Array.prototype.push.apply($scope.items,newItems)
            $scope.$broadcast('scroll.infiniteScrollComplete');

        };

    })




.filter('reverse', function () {
    return function (items) {
        return items.slice().reverse();
    };
})

.filter('capitalize', function() {
    return function(input, scope) {
        if (input!=null)
            input = input.toLowerCase();
        return input.substring(0,1).toUpperCase()+input.substring(1);
    }
})

function escapeEmailAddress(email) {
    if (!email)
        return false
    email = email.toLowerCase();
    email = email.replace(/\./g, ',');
    return email;
}

function unescapeEmailAddress(email) {
    if (!email)
        return false
    email = email.toLowerCase();
    email = email.replace(/\,/g, '.');
    return email;
}

