angular.module('starter.services', [])
    
        .factory('Items', ['$window', function($window) {
            return {

                all: function() {
                    var itemString = window.localStorage['items'];
                    if(itemString) {
                        return angular.fromJson(itemString);
                    }
                    return [];
                },
                save: function(items) {
                    window.localStorage['items'] = angular.toJson(items);
                },
                set: function(key, value) {
                    $window.localStorage[key] = value;
                },
                get: function(key, defaultValue) {
                    return $window.localStorage[key] || defaultValue;
                },
                setObject: function(key, value) {
                    $window.localStorage[key] = JSON.stringify(value);
                },
                getObject: function(key) {
                    return JSON.parse($window.localStorage[key] || '{}');
                }
            }
        }])

